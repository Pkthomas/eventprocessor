﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventProcessor.BL
{
    /// <summary>
    /// BackupAssistEvent Object
    /// </summary>
    public class BackupAssistEvent
    {
        public DateTime EventTime { get; set; }
        public string TaskName { get; set; }
        public string Destination { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Duration { get; set; }
        public string Message { get; set; }


        /// <summary>
        /// Create object BackupAssistEvent with  properties
        /// EventTime
        /// Duration
        /// Default Event 5634 > Unexpected Shutdown
        /// </summary>
        /// <param name="eventId"></param>
        public BackupAssistEvent(int eventId = 5634)
        {
            try
            {
                var baLog = new EventLog("Application", Environment.MachineName);
                var baLogEntries = new EventLogEntry[baLog.Entries.Count];
                baLog.Entries.CopyTo(baLogEntries, 0);
                var BackupAssistError = baLogEntries.Where(x => x.EventID == eventId).Where(x => x.Message.Contains("The Backup terminated unexpectedly")).Last();
                string[] stringSeparators = new string[] { "\n\r" };
                Message = BackupAssistError.Message;
                var eventItems = BackupAssistError.Message.Split(stringSeparators, StringSplitOptions.None);

                
                string[] taskSeparators = new string[] { "Backup job" };
                var taskvalues = eventItems[0].Split(taskSeparators, StringSplitOptions.None);
                taskSeparators = new string[] { "failed" };

                TaskName = taskvalues[1].Split(taskSeparators, StringSplitOptions.None)[0];

                var eventValues = eventItems[1].Split('\n');
                stringSeparators = new string[] { "Start time:" };
                Destination = eventValues[1].Split(':')[1];
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");

                StartTime = DateTime.Parse(eventValues[2].Split(stringSeparators, StringSplitOptions.None)[1], culture);
                stringSeparators = new string[] { "End time:" };
                EndTime = DateTime.Parse(eventValues[3].Split(stringSeparators, StringSplitOptions.None)[1], culture);
                stringSeparators = new string[] { "Duration:" };
                Duration = eventValues[4].Split(stringSeparators, StringSplitOptions.None)[1];
            }
            catch (Exception ex)
            {

                //log the exception. Here throwing the exception.
                throw ex;
            }
        }
    }

    internal enum BackupAssistReason
    {
        ShutDown,
        Crash,
        Invalid

    }
}
