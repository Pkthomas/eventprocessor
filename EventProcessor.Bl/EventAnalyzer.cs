﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventProcessor.BL
{
    /// <summary>
    /// Main object for Processing Events
    /// </summary>
    public class EventAnalyzer
    {
       
        public BackupAssistEvent BAEvent { get; set; }
        public SystemEvent SysEvent { get; set; }

        /// <summary>
        /// Default constructor Intializing BackupAssist Event & SytemEvent(Event Id :6008,System Crash).
        /// </summary>
        public EventAnalyzer()
        {

            BAEvent = new BackupAssistEvent();
            SysEvent = new SystemEvent();

        }
        /// <summary>
        /// Return Reason for Last Backup Assist Execution Termination.
        /// </summary>
        /// <returns></returns>
        public TaskErrorInfo  getReasopn()
        {

            TaskErrorInfo taskError = new TaskErrorInfo();
            if (SysEvent.EventTime > BAEvent.StartTime && SysEvent.EventTime < BAEvent.EndTime) { taskError.Reason = "System failure"; taskError.TaskName = BAEvent.TaskName; }
            else
            {
                SysEvent = new SystemEvent(6006);
                if (SysEvent.EventTime > BAEvent.StartTime && SysEvent.EventTime < BAEvent.EndTime) { { taskError.Reason = "Shutdown"; taskError.TaskName = BAEvent.TaskName; } }
                else { { taskError.Reason = "No Backupassit task was running on last system shutdown"; taskError.TaskName = BAEvent.TaskName; }}
            }
            return taskError;
        }
        }


    }
