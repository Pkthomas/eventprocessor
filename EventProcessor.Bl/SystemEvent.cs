﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventProcessor.BL
{
    public class SystemEvent
    {
        public DateTime EventTime { get; set; }
        public string Message { get; set; }

        /// <summary>
        /// Create object SystemEvent having property EventTime
        /// Default Event 6008 Unexpected Shutdown
        /// </summary>
        /// <param name="eventId"></param>
        public SystemEvent(int eventId = 6008)
        {
            try
            {
                var log = new EventLog("System", Environment.MachineName);
                var entries = new EventLogEntry[log.Entries.Count];
                log.Entries.CopyTo(entries, 0);
                EventLogEntry logEntry = entries.Where(x => x.EventID == eventId).Last();
                EventTime = logEntry.TimeGenerated;
                Message = logEntry.Message;
            }
            catch (Exception ex)
            {

                //log the exception. Here throwing the exception.
                throw ex;
            }
        }

    }
}
