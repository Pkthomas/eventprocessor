﻿namespace EventProcessor.BL
{
    public class TaskErrorInfo
    {
        public string TaskName { get; set; }
        public string Reason{ get; set; }
    }
}