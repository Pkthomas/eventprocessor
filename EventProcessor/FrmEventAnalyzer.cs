﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EventProcessor.BL;

namespace EventProcessor.UI
{
    public partial class FrmEventAnalyzer : Form
    {
        public FrmEventAnalyzer()
        {
            InitializeComponent();
        }

        private void btnEvents_Click(object sender, EventArgs e)
        {
            try
            {
                EventAnalyzer  eventProcessor = new EventAnalyzer();
                TaskErrorInfo errorInfo = eventProcessor.getReasopn();
                txtReason.Text += Environment.NewLine;
                txtReason.Text += "Corresponding BackupAssist event message";
                txtReason.Text += Environment.NewLine;
                txtReason.Text += "-------------------------------------------------------------";
                txtReason.Text += Environment.NewLine;
                txtReason.Text += eventProcessor.BAEvent.Message;
                txtReason.Text += Environment.NewLine;
                txtReason.Text += "Corresponding System event message";
                txtReason.Text += Environment.NewLine;
                txtReason.Text += "-------------------------------------------------------------";
                txtReason.Text += Environment.NewLine;
                txtReason.Text += eventProcessor.SysEvent.Message;
                txtReason.Text += Environment.NewLine;
                txtReason.Text += "-------------------------------------------------------------";
                txtReason.Text += Environment.NewLine;
                txtReason.Text += "Event analyzer message";
                txtReason.Text += Environment.NewLine;
                txtReason.Text += "-------------------------------------------------------------";
                txtReason.Text += Environment.NewLine;
             
                txtReason.Text += "BackupAssist error for task "+ errorInfo.TaskName + " - " + errorInfo.Reason;
                txtReason.Text += Environment.NewLine;
            }
            catch (Exception ex)
            {
                txtReason.Text += "Event Processor Exception";
                txtReason.Text += "-------------------------------------------------------------";
                txtReason.Text += Environment.NewLine;
                txtReason.Text += ex.StackTrace;

            }



        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtReason.Text = "";
        }
    }
}
